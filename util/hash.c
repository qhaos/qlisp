#include <stdio.h>
#include <stdint.h>

static char* strings[] = {
  "+",
  "-",
  "*",
  "/",
  "print",
  "def",
  "'"
};

uint64_t hash(uint8_t* str) {
  uint64_t hash = 5381;
  
  while(*str)
    hash = hash*33 + *(str++);

  return hash;
}

int main() {
  for(size_t i = 0; i < sizeof(strings)/sizeof(char*); i++)
    printf("%p /* %s */\n", hash(strings[i]), strings[i]);
}