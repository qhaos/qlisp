#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <ctype.h>

typedef enum type {
  TERR,
  TNUM,
  TSTR,
  TIDEN,
  TLIST,
  TARG,
  TNIL,
} type_t;

static const char* type_str[] = {
  [TERR]  "TERR",
  [TNUM]  "TNUM",
  [TSTR]  "TSTR",
  [TIDEN] "TIDEN",
  [TLIST] "TLIST",
  [TARG] "TARG",
  [TNIL] "TNIL",
};

struct list;

typedef struct value {
  union val {
    double d;
    char* str;
    struct list* list;
    uint64_t n;
  } val;
  type_t type :31;
  bool arg :1;
  uint32_t pos;
} value_t;

const value_t NIL = {{.str = NULL}, TNIL};

typedef struct list {
  struct list* next;
  struct list* prev;
  value_t value;
} list_t;

typedef struct var {
  struct var* next;
  struct var* prev;
  value_t value;
  uint64_t name;
} var_t;

var_t varhead = {NULL, NULL, {{.str = NULL}, TNIL}, 0};
var_t* vartail = &varhead;

enum ftype {
  BUILTIN,
  USERDEF,
};

typedef struct fun {
  uint64_t name;
  enum ftype tag;
  union {
    value_t (*pointer)(list_t*);
    list_t* body;
  } fun;
} fun_t;

typedef struct funlist {
  struct funlist* prev;
  struct funlist* next;
  fun_t fun;
} funlist_t;


/* builtins */
value_t op_add(list_t*);
value_t op_sub(list_t*);
value_t op_mul(list_t*);
value_t op_div(list_t*);
value_t op_print(list_t*);
value_t op_def(list_t*);
value_t op_quot(list_t*);

static funlist_t builtins[] = {
  {NULL,          &builtins[1], {0x000002b5d0 /* + */,      BUILTIN, op_add  }},
  {&builtins[0],  &builtins[2], {0x000002b5d2 /* - */,      BUILTIN, op_sub  }},
  {&builtins[1],  &builtins[3], {0x000002b5cf /* * */,      BUILTIN, op_mul  }},
  {&builtins[2],  &builtins[4], {0x000002b5d4 /* / */,      BUILTIN, op_div  }},
  {&builtins[3],  &builtins[5], {0x31102a0912 /* print */,  BUILTIN, op_print}},
  {&builtins[4],  &builtins[6], {0x000b8869b4 /* def */,    BUILTIN, op_def  }},
  {&builtins[5],  NULL,         {0x000002b5cc /* ' */,      BUILTIN, op_quot }},
};

static funlist_t* functions = &builtins[0];
funlist_t* funtail = &builtins[(sizeof(builtins)/sizeof(builtins[0]))-1];
value_t eval(list_t* head);

void data_list_add(list_t** this, value_t val) {
  list_t* head = *this;
  head->next = malloc(sizeof(list_t));
  memset(head->next, 0, sizeof(list_t));
  head->next->value = val;
  head->next->prev = head;
  (*this) = head->next;
  return;
}

void var_list_add(var_t val) {
  vartail->next = malloc(sizeof(var_t));
  memset(vartail->next, 0, sizeof(var_t));
  vartail->next->value = val.value;
  vartail->next->name = val.name;
  vartail->next->prev = vartail;
  vartail->next->next = NULL;
  vartail = vartail->next;
  return;
}

value_t data_list_get(list_t* this, size_t n) {
  list_t* curr;
  size_t i = 0;
  for(curr = this; curr && i < n; i++, curr = curr->next)
    ;
  return curr->value;
}

void funlist_add(fun_t fun) {
  funtail->next = malloc(sizeof(funlist_t));
  funtail->next->fun = fun;
  funtail->next->prev = funtail;
  funtail = funtail->next;
  funtail->next = NULL;
  return;
}

uint64_t hash(uint8_t* str) {
  uint64_t hash = 5381;
  
  while(*str)
    hash = hash*33 + *(str++);

  return hash;
}

void print_val(const value_t* const val);

void print_list(list_t* head) {
  putchar('(');
  for(list_t* curr = head; curr; curr = curr->next) {
    if(curr->value.type == TLIST) {
      print_list(curr->value.val.list);
    } else {
      print_val(&curr->value);
    }
    putchar(' ');
  }
  putchar('\b');
  putchar(')');
}

void print_val(const value_t* const val) {
  switch(val->type) {
    case TNUM:
      printf("%lf", val->val.d);
    break;
    case TSTR:
    case TIDEN:
      printf("%s", val->val.str);
    break;
    case TNIL:
      printf("NIL");
    break;
    case TLIST:
      print_list(val->val.list);
    break;
    case TARG:
      printf("var: $%llu", val->pos);
    break;
    case TERR:
      puts("err");
    break;
    default:
      puts("unknown type");
    break;
  }
}

value_t op_add(list_t* head) {
  value_t val = {{.d = 0}, TERR};
  if(!head) return val;
  switch(head->value.type) {
    case TNUM: {
      double acc = head->value.val.d;
      for(list_t* curr = head->next; curr; curr = curr->next) {
        if(curr->value.type != TNUM) {
          fputs("+ is not defined for mismatching types\n", stderr);
        } else {
          acc += curr->value.val.d;
        }
      }
      val.type = TNUM;
      val.val.d = acc;
    }
    break;
    case TSTR: {
      size_t len = strlen(head->value.val.str) + 1;
      char* acc =  malloc(len);
      strcpy(acc, head->value.val.str);
      for(list_t* curr = head->next; curr; curr = curr->next) {
        if(curr->value.type != TSTR) {
          fputs("+ is not defined for mismatching types\n", stderr);
        } else {
          len += strlen(curr->value.val.str);
          acc = realloc(acc, len);
          strcat(acc, curr->value.val.str);
        }
      }
      val.type = TSTR;
      val.val.str = acc;
    }
    break;
  }
  return val;
}

value_t op_sub(list_t* head) {
  value_t val = {{.d = 0}, TERR};
  if(!head) return val;
  switch(head->value.type) {
    case TNUM: {
      double acc = head->value.val.d;
      for(list_t* curr = head->next; curr; curr = curr->next) {
        if(curr->value.type != TNUM) {
          fputs("- is not defined for mismatching types\n", stderr);
        } else {
          acc -= curr->value.val.d;
        }
      }
      val.type = TNUM;
      val.val.d = acc;
    }
    break;
    default:
      fprintf(stderr, "error: - is not defined for %s\n", type_str[head->value.type]);
    break;
  }
  return val;
}

value_t op_mul(list_t* head) {
  value_t val = {{.d = 0}, TERR};
  if(!head) return val;
  switch(head->value.type) {
    case TNUM: {
      double acc = head->value.val.d;
      for(list_t* curr = head->next; curr; curr = curr->next) {
        if(curr->value.type != TNUM) {
          fputs("* is not defined for mismatching types\n", stderr);
        } else {
          acc *= curr->value.val.d;
        }
      }
      val.type = TNUM;
      val.val.d = acc;
    }
    break;
    default:
      fprintf(stderr, "error: * is not defined for %s\n", type_str[head->value.type]);
    break;
  }
  return val;
}

value_t op_div(list_t* head) {
  value_t val = {{.d = 0}, TERR};
  if(!head) return val;
  switch(head->value.type) {
    case TNUM: {
      double acc = head->value.val.d;
      for(list_t* curr = head->next; curr; curr = curr->next) {
        if(curr->value.type != TNUM) {
          fputs("+ is not defined for mismatching types\n", stderr);
        } else {
          acc /= curr->value.val.d;
        }
      }
      val.type = TNUM;
      val.val.d = acc;
    }
    break;
    default:
      fprintf(stderr, "error: / is not defined for %s\n", type_str[head->value.type]);
    break;
  }
  return val;
}

value_t op_print(list_t* head) {
  value_t val = {{0}, TNIL};

  for(list_t* curr = head; curr; curr = curr->next) {
    print_val(&curr->value);
    putchar(' ');
  }
  putchar('\n');
  return val;
}

value_t op_def(list_t* head) {
  var_t var = {.value = head->next->value,.name = hash(head->value.val.str)};
  var_list_add(var);
  return NIL;
}

value_t op_quot(list_t* head) {
  value_t val = {{.list = head}, TLIST};

  return val;
}

void defun(list_t* head) {
  fun_t fun;
  fun.tag = USERDEF;
  fun.name = hash(head->value.val.str);
  fun.fun.body = head->next->value.val.list;
  funlist_add(fun);
}

void recursive_replace(list_t* head, list_t* args) {
  for(list_t* curr = head; curr; curr = curr->next) {
    if(curr->value.type == TLIST) {
      recursive_replace(curr->value.val.list, args);
    } else if(curr->value.arg) {
      uint32_t pos = curr->value.pos;
      curr->value = data_list_get(args, pos);
      curr->value.arg = true;
      curr->value.pos = pos;
    }
  }

}

value_t apply(list_t* head) {
  bool matches = false;
  value_t ret = {{0}, TERR};

  if(!head->value.val.str)
    return NIL;

  uint64_t op = hash(head->value.val.str);

  for(funlist_t* curr = functions; curr; curr = curr->next) {
    if(op == curr->fun.name) {
      matches = true;
      if(curr->fun.tag == BUILTIN) {
        ret = curr->fun.fun.pointer(head->next);
      } else {
        list_t* call = curr->fun.fun.body;
        recursive_replace(call, head);
        ret = eval(call);
      }
      break;
    }
  }

  if(!matches)
    fprintf(stderr, "invalid op: \"%s\" (%p, %p)\n", head->value.val.str, head->value.val.str, op);
  return ret;
}

value_t parse(char* input, char** endptr) {
  while(isspace(*input))
    input++;

  list_t result = {NULL, NULL, {0.0, TERR}};

  list_t* tail = &result;

  while(*++input) {
    if(isspace(*input))
      continue;

    if(isdigit(*input) || *input == '.') {
      value_t val = {{strtod(input, &input)},TNUM};
      input--;

      data_list_add(&tail, val);
      continue;
    }
    switch(*input) {
      case '(':
        data_list_add(&tail, parse(input, &input));
      break;
      case '`': {
        value_t target = {{.str = NULL}, TSTR};
        char* str = ++input;
        while(*++str != '`');
        size_t len = str-input;
        char* mem = malloc(len+1);
        target.val.str = mem;
        strncpy(target.val.str, input, len);
        target.val.str[len] = 0;
        input = str;
        data_list_add(&tail, target);
      }
      break;
      case ')':
        goto end;
      break;
      case '$': {
        value_t param = {{.n = 0}, TARG, true, strtoul(++input, &input, 10)};
        input--;
        data_list_add(&tail, param);
      }
      break;
      default: {
        size_t idenlen = 0;
        while(*input && !isspace(*input) && *input != '(' && *input != ')')
          input++, idenlen++;
        char* idenstr = malloc(idenlen+1);
        idenstr[idenlen] = 0;
        strncpy(idenstr, input-idenlen, idenlen);
        value_t str = {{.str = idenstr}, TIDEN};
        data_list_add(&tail, str);
        input--;
      }
      break;
    }
  }
  end:
  if(endptr) {
    *endptr = input;
  }
  const value_t ret = {{.list = result.next}, TLIST};
  return ret;
}

value_t eval(list_t* head) {
  for(list_t* curr = head->next; curr; curr = curr->next) {
    if(curr->value.type == TLIST) {
      list_t* list = curr->value.val.list;
      if(list->value.type == TIDEN && !strncmp(list->value.val.str, "defun", 5)) {
        defun(list->next);
        curr->prev->next = curr->next;
      } else {
        curr->value = eval(list);
      }
    } else if(curr->value.type == TIDEN) {
      uint64_t name = hash(curr->value.val.str);
      for(var_t* vcurr = vartail; vcurr; vcurr = vcurr->prev){
        if(name == vcurr->name) {
          curr->value = vcurr->value;
          break;
        }
      }
    }
  }

  return apply(head);
}

int main(int argc, char* argv[]) {
  static char buff[2048];
  FILE* infile = argc > 1? fopen(argv[1], "r") : stdin;

  if(!infile) {
    perror("fopen");
    exit(1);
  }

  fread(buff, 1, sizeof(buff), infile);

  list_t* head = parse(buff, NULL).val.list;

  print_list(head);
  putchar('\n');
  value_t res = eval(head);
  print_val(&res);
  putchar('\n');
}